<?php
    session_start();
    include_once('../includes/connection.php');
    
if(isset($_SESSION['logged_in'])){
    if(isset($_POST['title'],$_POST['content'])){
        $title=$_POST['title'];
        $content=nl2br($_POST['content']);
    
        
        if(empty($title) or empty($content)) {
            $error='All fields are required!';
        }
        else{
            $query = $pdo->prepare('INSERT INTO articles (article_title,article_content,article_timestamp) VALUES(?,?,?)');
            
            $query->bindValue(1,$title);
            $query->bindValue(2,$content);
            $query->bindValue(3,time());
            
            $query->execute();
            header('Location: ../admin/index.php');
        }
    }
    
    
    ?>

     <html>
        <head>
            
            
		<title>CMS</title>
		
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/signup.css">
        
        <style>
            .navbar{
                margin-bottom: 0;
                border-radius: 0;
                background-color: #ff9d00;
                color: white;
                padding: 1% 0;
                font-size: 5es;
                border: 0;
            }
            .navbar-brand{
                float: left;
                min-height: 55px;
                padding: 0 15px 5px;
            }
            .navbar-inverse .navbar-nav .active a,.navbar-inverse .navbar-nav .active a:focus{
                color:darkgrey;
                background-color:#000000;        
            }
            .navbar-inverse .navbar-nav li a
            {
                color: white;
                font-size: 20px;
                
            }
            .navbar-inverse .navbar-nav li a:hover{
                color: gainsboro;
            }
            H1{
                align-content: center;
                color: white;
                padding-bottom: 50px;
            }
            .btn{
                font-size: 18px;
                color: white;
                padding: 12px 25px;
                background:#ff9d00;
                border: 2px solid;
            }
            
            .btn btn{
                padding: 25px;
            }
             footer{
                width: 100%;
                background-color: #ff9d00;
                color: white;
                padding: 3%;
            }
            .fa{
                font-size: 35px;
                padding: 20px;
                color: white;
            }
            .fa:hover{
                color: gainsboro;
                text-decoration: none;
            }
            .carousel-caption{
                top: 35%;
                transform: translateX(-50%);
                color: red;
                
            }
            .nav navbar-nav navbar-right{
                font-size: 50px;
            }
            @media(max-width:600px)
            {
                .carousel-caption{
                    display: none;
                    
                }
            }
            .container{
                width: 80%;
            }
        </style>
            
	   </head>
	<body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header"> 
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="a"><H1 >Blogger</H1></a>
                    
                </div>
            
                <div class="collapse navbar-collapse container-center " id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                    <li ><a href="../index.php">HOME</a></li>

                    <li><a href="../all_articles.php">ARTICLES</a></li>
                        <li><a href="admin/add.php">CREATE YOUR BLOG</a></li>
                        <li><a href="logout.php">LOGOUT</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
		<div class="container">
            <!--
            <?php if(isset($error)) {?>

                <small style="color:red;"><?php echo $error ?></small>
                <br>
                <br>
            <?php } ?>
            
            
            <form action="add.php" method="post" autocomplete="off">
            <input type="text" name="title" placeholder="Title"/><br><br>
                
                <textarea rows="15" cols="50" placeholder="Content" name="content"></textarea>
                <br><br>
                <input type="submit" value="Add Article" />
            </form>
            -->
        
		</div>
        <div class="container" >
            <div class="form-style-10">
  
                <h1 class="w3-wide">CREATE A BLOG</h1>
                <?php if(isset($error)) {?>

                    <small style="color:red;"><?php echo $error ?></small>
                    <br>
                    <br>
                <?php } ?>
                <form action="add.php" method="post" autocomplete="off">
    
                    <div class="section w3-wide">Title and Description</div>
                        <div class="inner-wrap">
                            <label>Enter Title<input type="text" name="title" placeholder="Title" required/></label>
                        </div>
    

                        <div class="section w3-wide">Blog Contents</div>
                            <div class="inner-wrap">
        
                                <label>Post an Image(optional)<input type="file" name="pic" ></label>
                                <label>Content<textarea id="content" rows="15" cols="50" name="content" required>
                                </textarea></label>  
                            </div>
  
      

  
                            <div class="grid-container">
                                <div class="button-section w3-opacity ">
                                    <input type="submit" value="Add Article"  />
                                    
                                 </div>
                            </div>
    
                </form>
            </div>
        </div>
    
<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("myDIV");
var btns = header.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
</script>
        
        <footer class="container-fluid text-center">
            <div class="row" id="r1">
                <div class="col-sm-4">
                <h3>CONTACT US</h3>
                    <br>
                <h4>Our Address and Contact Info</h4>
                
                </div>
                <div class="col-sm-4">
                    <h3>CONNECT</h3>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-linkedin"></a>
                    <a href="#" class="fa fa-youtube"></a>
                    
                
                </div>
                <div class="col-sm-4">
                <h3>CONTACT US</h3>
                    <br>
                <h4>Our Address and Contact Info</h4>
                
                </div>
            </div>
            <div class="row" id="r2">
            <h3 id="tpp">&copy; 2018 - A VIT Course Project</h3>
            </div>
        </footer>
        
	</body>
</html>
    <?php
    
    
}
?>
