<?php

include_once('includes/connection.php');
include_once('includes/article.php');
$article=new Article;

if(isset($_GET['id'])) {
    $id = $_GET['id'];
    $data =$article->fetch_data($id);
    ?>
    <html>
	<head>
		<title>Blogger</title>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css" />
        <style>
            .navbar{
                margin-bottom: 0;
                border-radius: 0;
                background-color: #ff9d00;
                color: white;
                padding: 1% 0;
                font-size: 5es;
                border: 0;
            }
            .navbar-brand{
                float: left;
                min-height: 55px;
                padding: 0 15px 5px;
            }
            .navbar-inverse .navbar-nav .active a,.navbar-inverse .navbar-nav .active a:focus{
                color:darkgrey;
                background-color:#000000;        
            }
            .navbar-inverse .navbar-nav li a
            {
                color: white;
                font-size: 20px;
                
            }
            .navbar-inverse .navbar-nav li a:hover{
                color: gainsboro;
            }
            #tp1{
                align-content: center;
                color: white;
                padding-bottom: 50px;
            }
            .btn{
                font-size: 18px;
                color: white;
                padding: 12px 25px;
                background:#ff9d00;
                border: 2px solid;
            }
            
            .btn btn{
                padding: 25px;
            }
             footer{
                width: 100%;
                background-color: #ff9d00;
                color: white;
                padding: 3%;
            }
            .fa{
                font-size: 35px;
                padding: 20px;
                color: white;
            }
            .fa:hover{
                color: gainsboro;
                text-decoration: none;
            }
            .carousel-caption{
                top: 35%;
                transform: translateX(-50%);
                color: red;
                
            }
            .nav navbar-nav navbar-right{
                font-size: 50px;
            }
            @media(max-width:600px)
            {
                .carousel-caption{
                    display: none;
                    
                }
            }
            #tp{
                padding-top: 20px;
                font-size: 28px;
            }
            .container{
                padding-left: 30px;
                padding-top: 30px;
                padding-bottom: 30px;
                -webkit-box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
                  -moz-box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
                  box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
                border-radius: 15px 15px 15px 15px;
            }
            small{
                font-size: 12px;
            }

        </style>
        
        
        
	</head>
	<body>
        
        
        <nav class="navbar navbar-inverse" >
            <div class="container-fluid" >
                <div class="navbar-header"> 
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="a"><H1 id="tp1">Blogger</H1></a>
                    
                </div>
            
                <div class="collapse navbar-collapse container-center " id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                    <li ><a href="index.php">HOME</a></li>

                    <li><a href="all_articles.php">ARTICLES</a></li>
                        <li><a href="admin/add.php">CREATE YOUR BLOG</a></li>
                        <li><a href="admin/index.php">LOGIN</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <br><br><br>
        
		<div class="container">
            <!--<a href="index.php" id="logo">CMS</a>-->
            <h1 style="color:#FF9D00;"> <?php echo $data ['article_title'];?></h1>
		      <h5> Posted
                <?php  
                    echo date('l jS',$data['article_timestamp']);
                ?></h5>
                <p id="tp"> <?php echo $data['article_content'];?></p>
                <a href="index.php" style="color:#FF9D00;font-size:30px;" >&larr;Back</a>
        </div>
        
        <br><br><br>
        
        <footer class="container-fluid text-center">
            <div class="row" id="r1">
                <div class="col-sm-4">
                <h3>CONTACT US</h3>
                    <br>
                <h4>Our Address and Contact Info</h4>
                
                </div>
                <div class="col-sm-4">
                    <h3>CONNECT</h3>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-linkedin"></a>
                    <a href="#" class="fa fa-youtube"></a>
                    
                
                </div>
                <div class="col-sm-4">
                <h3>CONTACT US</h3>
                    <br>
                <h4>Our Address and Contact Info</h4>
                
                </div>
            </div>
            <div class="row" id="r2">
            <h3 id="tpp">&copy; 2018 - A VIT Course Project</h3>
            </div>
        </footer>
        
        
	</body>
</html>
    <?php
    
    
}else{
    header('Location: index.php');
    exit();
}

?>