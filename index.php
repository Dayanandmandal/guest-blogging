<?php
    include_once('includes/connection.php');
    include_once('includes/article.php');
$article=new Article;
$articles =$article->fetch_all();
//echo md5('password'); 

?>
<html>
	<head>
		<title>CMS</title>
		
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <style>
            .navbar{
                margin-bottom: 0;
                border-radius: 0;
                background-color: #ff9d00;
                color: white;
                padding: 1% 0;
                font-size: 5es;
                border: 0;
            }
            .navbar-brand{
                float: left;
                min-height: 55px;
                padding: 0 15px 5px;
            }
            .navbar-inverse .navbar-nav .active a,.navbar-inverse .navbar-nav .active a:focus{
                color:darkgrey;
                background-color:#000000;        
            }
            .navbar-inverse .navbar-nav li a
            {
                color: white;
                font-size: 20px;
                
            }
            .navbar-inverse .navbar-nav li a:hover{
                color: gainsboro;
            }
            H1{
                align-content: center;
                color: white;
                padding-bottom: 50px;
            }
            .btn{
                font-size: 18px;
                color: white;
                padding: 12px 25px;
                background:#ff9d00;
                border: 2px solid;
            }
            
            .btn btn{
                padding: 25px;
            }
             footer{
                width: 100%;
                background-color: #ff9d00;
                color: white;
                padding: 3%;
            }
            .fa{
                font-size: 35px;
                padding: 20px;
                color: white;
            }
            .fa:hover{
                color: gainsboro;
                text-decoration: none;
            }
            .carousel-caption{
                top: 35%;
                transform: translateX(-50%);
                color: red;
                
            }
            .nav navbar-nav navbar-right{
                font-size: 50px;
            }
            @media(max-width:600px)
            {
                .carousel-caption{
                    display: none;
                    
                }
            }
        </style>
	</head>
	<body>
        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header"> 
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="a"><H1 >Blogger</H1></a>
                    
                </div>
            
                <div class="collapse navbar-collapse container-center " id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                    <li ><a href="index.php">HOME</a></li>

                    <li><a href="all_articles.php">ARTICLES</a></li>
                        <li><a href="admin/index.php">CREATE YOUR BLOG</a></li >                      
                    
                        
                        <li><a id="login" href="admin/index.php">LOGIN</a></li>
                
                    </ul>
                </div>
            </div>
        </nav>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"> </li>
                <li data-target="#myCarousel" data-slide-to="1"> </li>
                <li data-target="#myCarousel" data-slide-to="2"> </li>
             </ol>
            <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="slide1.PNG" width="100%">
                <div class="carousel-caption">
                </div>
                </div>
                <div class="item">
                    <img src="slide3.PNG".jpg width="100%">
                </div>
                <div class="item">
                <img src="slide4.PNG" width="100%">
                </div>
            </div>
            <!--- SLider Button--->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>   
        </div>
        
        <footer class="container-fluid text-center">
            <div class="row" id="r1">
                <div class="col-sm-4">
                <h3>CONTACT US</h3>
                    <br>
                <h4>Our Address and Contact Info</h4>
                
                </div>
                <div class="col-sm-4">
                    <h3>CONNECT</h3>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-linkedin"></a>
                    <a href="#" class="fa fa-youtube"></a>
                    
                
                </div>
                <div class="col-sm-4">
                <h3>CONTACT US</h3>
                    <br>
                <h4>Our Address and Contact Info</h4>
                
                </div>
            </div>
            <div class="row" id="r2">
            <h3 id="tpp">&copy; 2018 - A VIT Course Project</h3>
            </div>
        </footer>
	</body>
</html>
 